import React, { Component } from 'react'

export default class headers extends Component {
    render() {
        return (
            <div>
                <div className="navbar navbar-expand-lg navbar-dark bg-dark">
                    <div className="container">
                        <a className="navbar-brand" href="#">Start Bootstrap</a>
                        <div>
                            <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                                <li className="nav-item"><a className="nav-link active" aria-current="page" href="#">Home</a></li>
                                <li className="nav-item"><a className="nav-link" href="#">About</a></li>
                                <li className="nav-item"><a className="nav-link" href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
