import React, { Component } from 'react'
// import Headers from './headers'
import Headers from './Headers'
import Body from './Body'
import Item from './Item'
import Footer from './Footer'


export default class extends Component {
    render() {
        return (
            <div>
                <Headers />
                <Body />
                <Item />
                <Footer />
            </div>
        )
    }
}
