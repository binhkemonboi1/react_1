import React, { Component } from 'react'

export default class DemoState extends Component {
    state = {
        number: 1,
    };
    // this.setState: dùng để update những gtri của state
    handleIncrease = () => {
        this.setState(
            {
                number: this.state.number + 1,
            },
            () => {
                console.log("tăng", this.state.number);
            });
    };
    handleDecrease = () => {
        this.setState(
            {
                number: this.state.number - 1,
            },
            () => {
                console.log("giảm", this.state.number);
            });

    };
    handleChangeName = (name) => {
        this.setState({ user: name });
    };
    render = () => {
        // console.log("tăng", this.state.number);
        return (
            <div className="text-center">
                <button onClick={this.handleDecrease} className="btn btn-danger">-</button>
                <span className="display-4 mx-5">{this.state.number}</span>
                <button onClick={this.handleIncrease} className="btn btn-warning">+</button>

                <h2 className={`display-1 ${this.state.user == "Alice" ? "text-secondary " : "text-primary"}`}>{this.state.user}</h2>
                <button onClick={() => {
                    this.handleChangeName("Bob")
                }}
                    className="btn btn-primary">Change to Bob</button>

                <button onClick={() => {
                    this.handleChangeName("Alice")
                }}
                    className="btn btn-secondary">Change to Alice</button>
            </div>
        );
    }
};

