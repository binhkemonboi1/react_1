import logo from './logo.svg';
import './App.css';
import FunctionComponent from './DemoComponent/FunctionComponent/FunctionComponent';
import ClassComponent from './DemoComponent/ClassComponent/ClassComponent';
import DataBinding from './DataBinding/DataBinding';
import Ex_Layout from './Ex_Layout/Ex_Layout';
import RenderWithMap from './RenderWithMap/RenderWithMap';
import BaiTapThucHanhLayout from './BaiTapLayoutComponent/BaiTapThucHanhLayout';
import DemoState from './DemoState/DemoState';
import Ex_Car from './Ex_Car/Ex_Car';
import DemoProps from './DemoProps/DemoProps';

function App() {
  return (
    <div className="App">
      {/* jsx: html + js */}
      {/* <h1 className="App-logo">Hello React App</h1> */}

      {/*buoi 1 */}
      {/* <FunctionComponent/>
      <ClassComponent/> */}
      {/* buoi 2 */}
      {/* <Ex_Layout /> */}
      {/* <DataBinding /> */}
      {/* <RenderWithMap /> */}
      <BaiTapThucHanhLayout />
      {/* buoi 3 */}
      {/* <DemoState /> */}
      {/* <Ex_Car /> */}
      {/* <DemoProps /> */}
      {/* props reactjs */}
    </div>
  );
}

export default App;
