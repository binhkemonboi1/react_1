import React, { Component } from 'react'
import Usercard from './Usercard.js'
import { faker } from '@faker-js/faker';

export default class DemoProps extends Component {
    state = {
        user: "Alice",
    };
    // state ở đâu, this.setState ở đó
    handleChangeName = () => {
        this.setState({ user: faker.animal.dog() });
    };
    render() {
        return (
            <div>
                <h2>DemoProps</h2>
                <Usercard userData={this.state.user} title="Profile"
                    handleChange={this.handleChangeName} />
            </div>
        );
    }
}
